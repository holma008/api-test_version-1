﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Collections;
using Database3.Models;

namespace Database3.Controllers
{
    public class UserController : ApiController
    {
        //[HttpGet]
        //[Route("GetData")]
        //GET GetData
        /// <summary>
        /// Get all users
        /// </summary>
        /// <returns></returns>

        //GET api/User
        public ArrayList Get()
        {
            Connection cc = new Connection();
            return cc.GetUsers();
        }
        
        /// <summary>
        /// Get specific user by ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET api/User/5
        public User GetUser(int id)
        {
            Connection cc = new Connection();
            User user = cc.GetUser(id);
            if(user == null)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound));
            }
            return user;
        }
        //testing api call with diffreent parameters
/*
        /// <summary>
        /// Get specific user by Name
        /// </summary>
        /// <param name="Name"></param>
        /// <returns></returns>
        // GET api/User?Name=John
        public User GetUser(string name)
        {
            Connection cc = new Connection();
            User user = cc.GetUser(name);
            if (user == null)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound));
            }
            return user;
        }*/


        // POST api/values
        public HttpResponseMessage Post([FromBody]User value)
        {
            Connection cc = new Connection();
            int id;
            id = cc.saveUser(value);
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created);
            response.Headers.Location = new Uri(Request.RequestUri, String.Format("User/{0}", id));//string is api call to retrieve entry data
            return response;
        }
        
        // PUT api/User/5
        public HttpResponseMessage Put(int id, [FromBody]User value)
        {
            Connection cc = new Connection();
            bool Existed = false;
            Existed = cc.UpdateUser(id, value);

            HttpResponseMessage response;

            if (Existed)
            {
                response = Request.CreateResponse(HttpStatusCode.Found);
                return response;
            }
            else
            {
                response = Request.CreateResponse(HttpStatusCode.NotFound);
                return response;
            }
        }
         
        // DELETE api/values/5
        public HttpResponseMessage Delete(int id)
        {
            Connection cc = new Connection();
            bool Existed = false;
            Existed = cc.DeleteUser(id);

            HttpResponseMessage response;

            if (Existed)
            {
                response = Request.CreateResponse(HttpStatusCode.NoContent);
                return response;
            }
            else
            {
                response = Request.CreateResponse(HttpStatusCode.NotFound);
                return response;
            }
        }
        
    }
}
