﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Collections;
using Database3.Models;
using System.Configuration;
using System.Drawing;
using System.IO;

namespace Database3
{
    public class Connection
    {
        public ArrayList GetUsers()
        {
            SqlConnection connection;
            string connectionString = ConfigurationManager.ConnectionStrings["Studio2BDataBase"].ConnectionString;//Web.config file, DBConnections not in source control
            connection = new SqlConnection();
            try
            {
                connection.ConnectionString = connectionString;
                connection.Open();

                ArrayList UserArray = new ArrayList();
                SqlDataReader myReader = null;

                string query = "SELECT * FROM Test";
                SqlCommand cmd = new SqlCommand(query, connection);

                myReader = cmd.ExecuteReader();
                while (myReader.Read())
                {
                    User user = new User();
                    user.UserID = myReader.GetInt32(0);
                    user.Name = myReader.GetString(1);
                    user.Description = myReader.GetString(2);
                    //user.Signature = myReader.GetString(3);
                    //if (user.Signature == null)
                    //    user.Signature = "insert image here";
                    UserArray.Add(user);
                }

                return UserArray;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }
        public User GetUser(int id)
        {
            SqlConnection connection;
            string connectionString = ConfigurationManager.ConnectionStrings["Studio2BDataBase"].ConnectionString;
            connection = new SqlConnection();
            try
            {
                connection.ConnectionString = connectionString;
                connection.Open();

                User user = new User();
                SqlDataReader myReader = null;

                string query = "SELECT * FROM Test WHERE UserID = " + id.ToString();
                SqlCommand cmd = new SqlCommand(query, connection);

                myReader = cmd.ExecuteReader();
                if (myReader.Read())
                {
                    user.UserID = myReader.GetInt32(0);
                    user.Name = myReader.GetString(1);
                    user.Description = myReader.GetString(2);
                    //user.Signature = myReader.GetString(3);
                    //if (user.Signature == null)
                    //    user.Signature = "insert image here";
                    return user;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }
        //testing GET with other parameter than UserID
/*
        public User GetUser(string name)
        {
            SqlConnection connection;
            string connectionString = ConfigurationManager.ConnectionStrings["Studio2BDataBase"].ConnectionString;
            connection = new SqlConnection();
            try
            {
                connection.ConnectionString = connectionString;
                connection.Open();

                User user = new User();
                SqlDataReader myReader = null;

                string query = "SELECT * FROM Test WHERE Name = '" + name + "'";
                SqlCommand cmd = new SqlCommand(query, connection);

                myReader = cmd.ExecuteReader();
                if (myReader.Read())
                {
                    user.UserID = myReader.GetInt32(0);
                    user.Name = myReader.GetString(1);
                    user.Description = myReader.GetString(2);
                    //user.Signature = myReader.GetString(3);
                    //if (user.Signature == null)
                    //    user.Signature = "insert image here";
                    return user;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }
        */




        public bool DeleteUser(int id)
        {
            SqlConnection connection;
            string connectionString = ConfigurationManager.ConnectionStrings["Studio2BDataBase"].ConnectionString;
            connection = new SqlConnection();
            try
            {
                connection.ConnectionString = connectionString;
                connection.Open();

                SqlDataReader myReader = null;

                string query = "SELECT * FROM Test WHERE UserID = " + id.ToString();
                SqlCommand cmd = new SqlCommand(query, connection);

                myReader = cmd.ExecuteReader();
                if (myReader.Read())
                {
                    myReader.Close();

                    query = "DELETE FROM Test WHERE UserID = " + id.ToString();
                    cmd = new SqlCommand(query, connection);

                    cmd.ExecuteNonQuery();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }

        public bool UpdateUser(int id, User user)
        {
            SqlConnection connection;
            string connectionString = ConfigurationManager.ConnectionStrings["Studio2BDataBase"].ConnectionString;
            connection = new SqlConnection();
            try
            {
                connection.ConnectionString = connectionString;
                connection.Open();

                SqlDataReader myReader = null;

                string query = "SELECT * FROM Test WHERE UserID = " + id.ToString();
                SqlCommand cmd = new SqlCommand(query, connection);

                myReader = cmd.ExecuteReader();
                if (myReader.Read())
                {
                    myReader.Close();

                    /*user.Signature*/Bitmap sig = new Bitmap("C:\\Users\\bholm\\OneDrive\\Documents\\CS 481\\HW3_1.png");//retrieves local image as bitmap
                    var ms = new MemoryStream();//create memory stream, this is where the bytes are deposited
                    /*user.Signature.Save*/sig.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);//save bytes to memory stream variable ms
                    user.Signature = ms.ToArray();
                    //var bytes = ms.ToArray();//to get the bytes
                    //HttpPostedFile sig = user.Signature
                    query = "UPDATE Test SET Name = '" + user.Name + "', Description = '" + user.Description + "', Signature = @Signature WHERE  UserID = " + id.ToString();//, Signature = '" + user.Signature + "' WHERE UserID = " + id.ToString();
                    cmd = new SqlCommand(query, connection);
                    cmd.Parameters.Add(new SqlParameter("@Signature", user.Signature));//why like this though?, why not just user.Signature in the query line?

                    cmd.ExecuteNonQuery();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }

        //not fully functional, do not use
        public int saveUser(User userToSave)
        {
            SqlConnection connection;
            string connectionString = ConfigurationManager.ConnectionStrings["Studio2BDataBase"].ConnectionString;
            connection = new SqlConnection();
            try
            {
                connection.ConnectionString = connectionString;
                connection.Open();

                string query = "INSERT INTO Test (Name, Description, Signature) VALUES ('" + userToSave.Name + "','" + userToSave.Description + "')";//,'" + userToSave.Signature + "')";
                SqlCommand cmd = new SqlCommand (query, connection);
                cmd.ExecuteNonQuery();
                //int id = cmd.LastInsertedId;
                int id = 1;
                return id;
            }
            catch(Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        } 
    }
}