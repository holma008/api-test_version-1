﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;

namespace Database3.Models
{
    public class User
    {
        public int UserID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        //public Bitmap Signature { get; set; }
        public byte[] Signature { get; set; }//convert image to byte in order to store in database?
    }
}